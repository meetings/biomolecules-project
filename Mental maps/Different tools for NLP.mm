<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1592563527922" ID="ID_165473596" MODIFIED="1592563540992" TEXT="Different tools for NLP">
<node CREATED="1592563581468" ID="ID_867966410" MODIFIED="1592563716163" POSITION="left" TEXT="Parsers">
<node CREATED="1592563593288" FOLDED="true" ID="ID_224848621" MODIFIED="1592563716921" TEXT="OPSIN - Open Parser for Systematic IUPAC Nomenclature   ">
<node CREATED="1592563617275" ID="ID_734399925" MODIFIED="1592563618422" TEXT=" https://github.com/dan2097/opsin "/>
</node>
<node CREATED="1592563717504" FOLDED="true" ID="ID_1628629692" MODIFIED="1592563753474" TEXT="ANTLR (ANother Tool for Language Recognition)">
<node CREATED="1592563729668" ID="ID_130696506" MODIFIED="1592563730918" TEXT="https://www.antlr.org"/>
</node>
</node>
</node>
</map>
