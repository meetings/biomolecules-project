<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1592916244060" ID="ID_265724180" MODIFIED="1592916556442" TEXT="BioCreative tasks">
<node CREATED="1592916258401" ID="ID_940769174" MODIFIED="1592916323607" POSITION="right" TEXT="BioCreative IV (2015)">
<node CREATED="1592916332684" ID="ID_481153838" MODIFIED="1592920277106" TEXT="Track 1- BioC: The BioCreative Interoperability Initiative [2012-11-15]  CHEMDNER (Task 2) FAQ [2013-08-01]  Track 2- CHEMDNER Task: Chemical compound and drug name recognition task [2012-11-15]  CHEMDNER (Task 2) Training Set [2013-08-01]  Track 3- BioCreative 2013 CTD Track [2012-11-15]  Track 4- GO Task [2012-11-15]  IAT (Task 5) For Biocurators [2013-07-22]  IAT (Task 5) Systems Descriptions [2013-07-25]  Track 5- User Interactive Task (IAT) [2012-11-15]  Track 5 - IAT Activity Workflow [2013-08-29]">
<node CREATED="1592916346300" ID="ID_819348803" MODIFIED="1592916348437" TEXT=" Chemical compound and drug name recognition "/>
</node>
</node>
<node CREATED="1592916270026" ID="ID_1903996409" MODIFIED="1592916684275" POSITION="right" TEXT="BioCreative II (2006)">
<node CREATED="1592916358711" FOLDED="true" ID="ID_712405595" MODIFIED="1592920245750" TEXT="Task 1A: Gene Mention Tagging [2006-04-01]  Task 1B: Human Gene Normalizations [2006-04-01]  Task 2: Protein-Protein Interactions [2006-04-01]">
<node CREATED="1592916364972" ID="ID_364818949" MODIFIED="1592916397960" TEXT="Gene and protein name recognition"/>
</node>
</node>
<node CREATED="1592916558330" ID="ID_1045848185" MODIFIED="1592916760159" POSITION="left" TEXT="BioCreative I (2004)">
<node CREATED="1592916697251" ID="ID_1581295933" MODIFIED="1592920204685" TEXT="Task 1A: Gene Mention Identification [2004-01-02]  Task 1B: Gene Normalizations [2004-01-02]  Task 2: Functional Annotations [2004-01-03]"/>
</node>
<node CREATED="1592916565347" ID="ID_1731232010" MODIFIED="1592916802147" POSITION="left" TEXT="BioCreative III (2010)">
<node CREATED="1592916815249" ID="ID_1441582595" MODIFIED="1592920262064" TEXT="GN: Gene Normalization [2010-03-31]&#xa;&#xa;IAT: Interactive Demostration Task for Gene Indexing and Retrieval [2010-07-22]&#xa;&#xa;PPI: Protein-Protein Interactions [2009-12-08]"/>
</node>
<node CREATED="1592916574122" ID="ID_313631854" MODIFIED="1592917008337" POSITION="right" TEXT="BioCreative V (2014-2015)">
<node CREATED="1592920188933" ID="ID_1108754112" MODIFIED="1592920191014" TEXT="Track 4- BEL Task [2015-01-23]  Track 1- Collaborative Biocurator Assistant Task (BioC) [2014-10-21]  Track 3- CDR [2014-12-18]  CEMP detailed task description [2015-07-01]  Track 2- CHEMDNER-patents [2015-08-29]  IAT (Task 5) For Biocurators [2015-05-08]  Track 5- User Interactive Task (IAT) [2014-12-18]"/>
</node>
<node CREATED="1592916582838" ID="ID_1736358001" MODIFIED="1592916593433" POSITION="left" TEXT="BioCreative VI">
<node CREATED="1592920298814" ID="ID_488303147" MODIFIED="1592920300447" TEXT="PM-task-PPIregions [2017-03-03]  Track 1: Interactive Bio-ID Assignment (IAT-ID) [2017-02-06]  Track 2: Text-mining services for Human Kinome Curation [2017-02-06]  Track 3: Extraction of causal network information using the Biological Expression Language (BEL) [2017-02-06]  Track 4: Mining protein interactions and mutations for precision medicine (PM) [2017-03-03]  Track 5: Text mining chemical-protein interactions [2017-11-21]"/>
</node>
<node CREATED="1592916596372" ID="ID_1980368536" MODIFIED="1592916602033" POSITION="right" TEXT="BioCreative VII">
<node CREATED="1592920318019" ID="ID_288483687" MODIFIED="1592920319434" TEXT="Track 3 - Automatic extraction of medication names in tweets [2020-01-22]"/>
</node>
</node>
</map>
