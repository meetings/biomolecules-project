<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1591961886393" ID="ID_1798038771" MODIFIED="1591962781086" TEXT="ChemNER">
<node CREATED="1591961912984" FOLDED="true" ID="ID_1982789345" MODIFIED="1592486798132" POSITION="right" TEXT="Dictionary-based">
<node CREATED="1592376819417" ID="ID_398405081" MODIFIED="1592376822334" TEXT="Hawizy, L., Jessop, D. M., Adams, N., &amp; Murray-Rust, P. (2011). ChemicalTagger: A tool for semantic text-mining in chemistry. Journal of cheminformatics, 3(1), 17. ISO 690&#x9; "/>
<node CREATED="1592410976868" ID="ID_406679368" MODIFIED="1592410979386" TEXT="Akhondi, S. A., Hettne, K. M., Van Der Horst, E., Van Mulligen, E. M., &amp; Kors, J. A. (2015). Recognition of chemical entities: combining dictionary-based and grammar-based approaches. Journal of cheminformatics, 7(1), S10."/>
<node CREATED="1592410986004" ID="ID_1007219483" MODIFIED="1592410987275" TEXT="Irmer, M., Bobach, C., B&#xf6;hme, T., Laube, U., P&#xfc;schel, A., &amp; Weber, L. (2013, October). Chemical named entity recognition with ocminer. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 92). "/>
</node>
<node CREATED="1591961928125" FOLDED="true" ID="ID_1422853513" MODIFIED="1592487927391" POSITION="left" TEXT="Rule-based">
<node CREATED="1592410074392" ID="ID_316264030" MODIFIED="1592410076244" TEXT="Lowe, D. M., &amp; Sayle, R. A. (2015). LeadMine: a grammar and dictionary driven approach to entity recognition. Journal of cheminformatics, 7(1), S5. "/>
<node CREATED="1592410621669" ID="ID_1313220807" MODIFIED="1592410623457" TEXT="Ramanan, S., &amp; Nathan, P. S. (2013, October). Adapting cocoa, a multi-class entity detector, for the chemdner task of biocreative iv. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 60)."/>
<node CREATED="1592466521858" ID="ID_959435464" MODIFIED="1592466582495" TEXT="Lana-Serrano, S., Sanchez-Cisneros, D., Campillos, L., &amp; Segura-Bedmar, I. (2013, October). Recognizing chemical compounds and drugs: a rule-based approach using semantic information. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 121)."/>
</node>
<node CREATED="1591961946411" ID="ID_955296085" MODIFIED="1591961985770" POSITION="right" TEXT="Machine learning-based">
<node CREATED="1591961985771" FOLDED="true" ID="ID_1314099547" MODIFIED="1592487948177" TEXT="CRF">
<node CREATED="1591962125091" ID="ID_655922809" MODIFIED="1591962128515" TEXT="Campos, D., Matos, S., &amp; Oliveira, J. L. (2015). A document processing pipeline for annotating chemical entities in scientific documents. Journal of cheminformatics, 7(1), S7."/>
<node CREATED="1592409925171" ID="ID_1525500160" MODIFIED="1592409927420" TEXT="Batista-Navarro, R., Rak, R., &amp; Ananiadou, S. (2015). Optimising chemical named entity recognition with pre-processing analytics, knowledge-rich features and heuristics. Journal of cheminformatics, 7(1), S6."/>
<node CREATED="1592410530335" ID="ID_1735455801" MODIFIED="1592410531662" TEXT="Munkhdalai, T., Li, M., Batsuren, K., Park, H. A., Choi, N. H., &amp; Ryu, K. H. (2015). Incorporating domain knowledge in chemical and biomedical named entity recognition with word representations. Journal of cheminformatics, 7(S1), S9."/>
<node CREATED="1592465761046" ID="ID_960187302" MODIFIED="1592465764696" TEXT="Choi, M., Yepes, A. J., Zobel, J., &amp; Verspoor, K. (2013, October). Neroc: Named entity recognizer of chemicals. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 97)."/>
<node CREATED="1592465841640" ID="ID_1264092744" MODIFIED="1592465843002" TEXT="Khabsa, M., &amp; Giles, C. L. (2015). Chemical entity extraction using CRF and an ensemble of extractors. Journal of cheminformatics, 7(1), S12."/>
<node CREATED="1592466336107" ID="ID_519868487" MODIFIED="1592466374860" TEXT="Shu, C. Y., Lai, P. T., Wu, C. Y., Dai, H. J., &amp; Tsai, R. T. H. (2013, October). A chemical compound and drug named recognizer for biocreative iv. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 168)."/>
<node CREATED="1592466462283" ID="ID_523619975" MODIFIED="1592466464029" TEXT="Usi&#xe9;, A., Cruz, J., Comas, J., Solsona, F., &amp; Alves, R. (2015). CheNER: a tool for the identification of chemical entities and their classes in biomedical literature. Journal of cheminformatics, 7(1), S15."/>
</node>
<node CREATED="1591961990927" FOLDED="true" ID="ID_411100803" MODIFIED="1592468039286" TEXT="DL">
<node CREATED="1591961997366" ID="ID_1216111694" MODIFIED="1591962029601" TEXT="Awan, Z., Kahlke, T., Ralph, P. J., &amp; Kennedy, P. J. (2019). Chemical Named Entity Recognition with Deep Contextualized Neural Embeddings."/>
<node CREATED="1591962162382" ID="ID_1094341874" MODIFIED="1591962163609" TEXT="Luo, L., Yang, Z., Yang, P., Zhang, Y., Wang, L., Wang, J., &amp; Lin, H. (2018). A neural network approach to chemical and gene/protein entity recognition in patents. Journal of cheminformatics, 10(1), 65."/>
</node>
</node>
<node CREATED="1591961961334" ID="ID_635880652" MODIFIED="1591961975861" POSITION="left" TEXT="Hybrid">
<node CREATED="1592409353223" FOLDED="true" ID="ID_481081986" MODIFIED="1592466111267" TEXT="SVM+CRF+Rule-based">
<node CREATED="1592409403609" ID="ID_805574621" MODIFIED="1592409405541" TEXT="Leaman, R., Wei, C. H., &amp; Lu, Z. (2015). tmChem: a high performance approach for chemical named entity recognition and normalization. Journal of cheminformatics, 7(S1), S3."/>
</node>
<node CREATED="1592410258556" FOLDED="true" ID="ID_1948541150" MODIFIED="1592466110168" TEXT="CRF + rule-based">
<node CREATED="1592410265976" ID="ID_1297435130" MODIFIED="1592410267261" TEXT="Ata, C., &amp; Can, T. (2013, October). Dbchem: A database query based solution for the chemical compound and drug name recognition task. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 42)."/>
</node>
<node CREATED="1592409695602" FOLDED="true" ID="ID_1469410258" MODIFIED="1594038529480" TEXT="CRF + clustering">
<node CREATED="1592409709991" ID="ID_793228468" MODIFIED="1592409711386" TEXT="Lu, Y., Ji, D., Yao, X., Wei, X., &amp; Liang, X. (2015). CHEMDNER system with mixed conditional random fields and multi-scale word clustering. Journal of cheminformatics, 7(S1), S4."/>
</node>
<node CREATED="1592410348546" FOLDED="true" ID="ID_1560902112" MODIFIED="1592468027265" TEXT="CRF + SVM">
<node CREATED="1592410419151" ID="ID_1543789340" MODIFIED="1592410421151" TEXT="Tang, B., Feng, Y., Wang, X., Wu, Y., Zhang, Y., Jiang, M., ... &amp; Xu, H. (2015). A comparison of conditional random fields and structured support vector machines for chemical entity recognition in biomedical literature. Journal of cheminformatics, 7(S1), S8. "/>
<node CREATED="1592410763908" ID="ID_23791596" MODIFIED="1592410765548" TEXT="Zitnik, S., &amp; Bajec, M. (2013, October). Token-and constituent-based linear-chain crf with svm for named entity recognition. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 144)."/>
<node CREATED="1592465677966" ID="ID_827910249" MODIFIED="1592465679808" TEXT="Sikdar, U. K., Ekbal, A., &amp; Saha, S. (2013, October). Domain-independent model for chemical compound and drug name recognition. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 158)."/>
<node CREATED="1592466225411" ID="ID_1050796554" MODIFIED="1592466305625" TEXT="Li, L., Guo, R., Liu, S., Zhang, P., Zheng, T., Huang, D., &amp; Zhou, H. (2013, October). Combining machine learning with dictionary lookup for chemical compound and drug name recognition task. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 171)."/>
</node>
<node CREATED="1592465013276" FOLDED="true" ID="ID_513407673" MODIFIED="1592466101056" TEXT="CRF + Max entropy">
<node CREATED="1592465024338" ID="ID_1095976621" MODIFIED="1592465025795" TEXT="Xu, S., An, X., Zhu, L., Zhang, Y., &amp; Zhang, H. (2015). A CRF-based system for recognizing chemical entity mentions (CEMs) in biomedical literature. Journal of cheminformatics, 7(S1), S11."/>
</node>
<node CREATED="1592465856506" FOLDED="true" ID="ID_958812414" MODIFIED="1592466093731" TEXT="CRF + SVM + LR">
<node CREATED="1592465872314" ID="ID_893343594" MODIFIED="1592465955946" TEXT="Ravikumar, K., Li, D., Jonnalagadda, S., Wagholikar, K. B., Xia, N., &amp; Liu, H. (2013, October). An ensemble approach for chemical entity mention detection and indexing. In BioCreative Challenge Evaluation Workshop (Vol. 2, p. 140)."/>
</node>
<node CREATED="1592466094628" FOLDED="true" ID="ID_70782512" MODIFIED="1592468029372" TEXT="CRF + RF">
<node CREATED="1592466121640" ID="ID_399827381" MODIFIED="1592466194951" TEXT="Lamurias, A., Ferreira, J. D., &amp; Couto, F. M. (2015). Improving chemical entity recognition through h-index based semantic similarity. Journal of cheminformatics, 7(S1), S13."/>
</node>
</node>
<node CREATED="1591962185726" FOLDED="true" ID="ID_702210083" MODIFIED="1592468040645" POSITION="right" TEXT="Reviews and Surveys">
<node CREATED="1591962238505" ID="ID_1625985990" MODIFIED="1591962240428" TEXT="Eltyeb, S., &amp; Salim, N. (2014). Chemical named entities recognition: a review on approaches and applications. Journal of cheminformatics, 6(1), 17."/>
<node CREATED="1591962308848" ID="ID_1171214210" MODIFIED="1591962310606" TEXT="Krallinger, M., Leitner, F., Rabal, O., Vazquez, M., Oyarzabal, J., &amp; Valencia, A. (2015). CHEMDNER: The drugs and chemical names extraction challenge. Journal of cheminformatics, 7(1), S1."/>
</node>
<node CREATED="1591962786326" FOLDED="true" ID="ID_1255732338" MODIFIED="1592468030407" POSITION="left" TEXT="Pre-processing">
<node CREATED="1591962796417" ID="ID_1547724564" MODIFIED="1591962803815" TEXT="Text cleaning"/>
<node CREATED="1591962806851" ID="ID_1493221076" MODIFIED="1591962812352" TEXT="Sentence splitting"/>
<node CREATED="1591962815231" ID="ID_1550023347" MODIFIED="1591962822034" TEXT="Tokenization"/>
</node>
<node CREATED="1591962843847" FOLDED="true" ID="ID_643475543" MODIFIED="1592468041552" POSITION="right" TEXT="Post-processing">
<node CREATED="1591962859857" ID="ID_1376882881" MODIFIED="1591962867050" TEXT="Tagging consistency"/>
<node CREATED="1591962869970" ID="ID_152574308" MODIFIED="1591962876626" TEXT="Abbreviation resolution"/>
<node CREATED="1591962879232" ID="ID_759604999" MODIFIED="1591962886029" TEXT="Bracket balance"/>
</node>
</node>
</map>
