<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1591887955758" ID="ID_529260262" MODIFIED="1591961806500" TEXT="Chemical entities representation">
<node CREATED="1591887984966" ID="ID_277677294" MODIFIED="1591888268850" POSITION="right" TEXT="Information extraction">
<node CREATED="1591888276432" FOLDED="true" ID="ID_1312048590" MODIFIED="1594038491778" TEXT="NER">
<node CREATED="1591888214124" ID="ID_904731757" MODIFIED="1591888228987" TEXT="Dictionary-based">
<node CREATED="1591888232433" ID="ID_628267823" MODIFIED="1591888239199" TEXT="Exact matching"/>
<node CREATED="1591888242867" ID="ID_1442617679" MODIFIED="1591888517140" TEXT="Flexible matching"/>
</node>
<node CREATED="1591888336443" ID="ID_605421171" MODIFIED="1591888342373" TEXT="Rules-based">
<node CREATED="1591888345100" ID="ID_673571895" MODIFIED="1591888348067" TEXT="Pattern-based"/>
<node CREATED="1591888351308" ID="ID_1756845857" MODIFIED="1591888356154" TEXT="Context-based"/>
</node>
<node CREATED="1591888360312" ID="ID_994303303" MODIFIED="1591888367941" TEXT="Machine learning-based">
<node CREATED="1591888370518" ID="ID_58444218" MODIFIED="1591888373911" TEXT="Supervised">
<node CREATED="1591888390156" ID="ID_1624599090" MODIFIED="1591888393911" TEXT="CRF"/>
<node CREATED="1591888395893" ID="ID_1549811817" MODIFIED="1591888397192" TEXT="SVM"/>
<node CREATED="1591888401087" ID="ID_1812928613" MODIFIED="1591888480930" TEXT="HMM"/>
</node>
<node CREATED="1591888377782" ID="ID_1139795376" MODIFIED="1591888380962" TEXT="Unsupervised">
<node CREATED="1591888384379" ID="ID_1804283251" MODIFIED="1591888386946" TEXT="Clustering"/>
</node>
<node CREATED="1591888483095" ID="ID_328179191" MODIFIED="1591888487073" TEXT="Semi-supervised">
<node CREATED="1591888539751" ID="ID_1334163926" MODIFIED="1591888543568" TEXT="Boostrapping"/>
</node>
</node>
<node CREATED="1591888549923" ID="ID_1968050555" MODIFIED="1591888552799" TEXT="Hybrid"/>
<node CREATED="1592377351570" ID="ID_477212730" MODIFIED="1592377358270" TEXT="Deep Learning-based">
<node CREATED="1592377361571" ID="ID_370472373" MODIFIED="1592377366734" TEXT="Transfer learning"/>
<node CREATED="1592377370128" ID="ID_816148129" MODIFIED="1592377373153" TEXT="Fine tuning"/>
<node CREATED="1592377376302" ID="ID_1129758062" MODIFIED="1592377389805" TEXT="Multi/single/dependent task"/>
<node CREATED="1592377404283" ID="ID_1172773421" MODIFIED="1592377416398" TEXT="Architectures">
<node CREATED="1592377421517" ID="ID_556016006" MODIFIED="1592377424840" TEXT="Bi-LSTM"/>
<node CREATED="1592377428099" ID="ID_1285321119" MODIFIED="1592377430001" TEXT="CNN"/>
</node>
</node>
</node>
<node CREATED="1591888279085" ID="ID_1245605909" MODIFIED="1591888334189" TEXT="RE"/>
<node CREATED="1591888315531" ID="ID_1410666466" MODIFIED="1591888317651" TEXT="EE"/>
</node>
<node CREATED="1591888624134" ID="ID_1092422138" MODIFIED="1594038519403" POSITION="left" TEXT="Expressing methods for chemical structure information">
<node CREATED="1591888718303" ID="ID_1201687506" MODIFIED="1592380775716" TEXT="Systematic names">
<node CREATED="1591888848107" FOLDED="true" ID="ID_1463046606" MODIFIED="1594038461956" TEXT="reflect the information of the chemical structure. International Union of Pure and Applied Chemistry (IUPACh) ">
<node CREATED="1591889351171" ID="ID_602854035" MODIFIED="1591889354157" TEXT="&#x2018;3-(3,4-dihydroxyphenyl)prop-2-enoic acid&#x2019; "/>
</node>
</node>
<node CREATED="1591888727317" ID="ID_472274428" MODIFIED="1592380783399" TEXT="Trivial names">
<node CREATED="1591889147758" FOLDED="true" ID="ID_157375132" MODIFIED="1594038464568" TEXT="do not reflect the structure of the chemical substance.">
<node CREATED="1591889363169" ID="ID_899172278" MODIFIED="1591889365007" TEXT="&#x2018;caffeic acid&#x2019; utilized for &#x2018;3-(3,4-dihydroxyphenyl)prop-2-enoic acid&#x2019;."/>
</node>
</node>
<node CREATED="1591888734905" ID="ID_1019080322" MODIFIED="1592380784487" TEXT="Semi systematic names">
<node CREATED="1591889165483" FOLDED="true" ID="ID_144144247" MODIFIED="1594038466037" TEXT="at least one part is used in the systematic sense, IUPAC-like, non-IUPAC names. ">
<node CREATED="1591889372107" ID="ID_611686912" MODIFIED="1591889374487" TEXT="in&#x2018;N-benzoylglycine&#x2019; the part &#x2018;benzoyl&#x2019; is systematic, whereas &#x2018;glycine&#x2019; is the trivialname for &#x2018;_-aminoacetic acid&#x2019;"/>
</node>
</node>
<node CREATED="1591888742958" ID="ID_308532495" MODIFIED="1592380785531" TEXT="Common or generic names">
<node CREATED="1591889197063" FOLDED="true" ID="ID_1344364135" MODIFIED="1594038467378" TEXT="names applied to a class of compounds">
<node CREATED="1591889383877" ID="ID_595894447" MODIFIED="1591889387711" TEXT="camphor, water and alcohol"/>
</node>
</node>
<node CREATED="1591888754650" ID="ID_1575217330" MODIFIED="1592380786606" TEXT="Registered trademark/brand names">
<node CREATED="1591889209189" FOLDED="true" ID="ID_1436188614" MODIFIED="1594038469183" TEXT="they identify the brand owner as the commercial source of products.">
<node CREATED="1591889394012" ID="ID_667745354" MODIFIED="1591889397078" TEXT="&#x2018;aspirin&#x2019;"/>
</node>
</node>
<node CREATED="1591888772129" ID="ID_1440861656" MODIFIED="1592380788814" TEXT="Company codes">
<node CREATED="1591889220627" FOLDED="true" ID="ID_1527414734" MODIFIED="1594038470564" TEXT="a company code is to identify the compound within the company.">
<node CREATED="1591889404613" ID="ID_1928669862" MODIFIED="1591889406648" TEXT="ZD5077 = ICI204636 = ZM204636"/>
</node>
</node>
<node CREATED="1591888778218" ID="ID_253239842" MODIFIED="1592380787767" TEXT="Acronyms and abbreviations">
<node CREATED="1591889232494" FOLDED="true" ID="ID_1597637751" MODIFIED="1594038471751" TEXT="they are used to get short names.">
<node CREATED="1591889413764" ID="ID_1847705247" MODIFIED="1591889416529" TEXT="DMS for dimethyl sulfate"/>
</node>
</node>
<node CREATED="1591888791564" ID="ID_440719346" MODIFIED="1592380789757" TEXT="index and reference">
<node CREATED="1591889250223" FOLDED="true" ID="ID_283777938" MODIFIED="1594038472940" TEXT="numbers from Chemical Abstracts Service (CAS) registry numbers, Beilstein registry numbers, etc">
<node CREATED="1591889424298" ID="ID_1901472050" MODIFIED="1591889429735" TEXT="CAS number of water is 7732-18-5"/>
</node>
</node>
<node CREATED="1591888803741" ID="ID_703799701" MODIFIED="1592380790789" TEXT="Anaphors">
<node CREATED="1591889267064" FOLDED="true" ID="ID_1874636378" MODIFIED="1591889623635" TEXT=" Compounds are named earlier in the text but co-referenced to a shorter name, called the anaphor, later in the text.">
<node CREATED="1591889438683" ID="ID_623434337" MODIFIED="1591889442058" TEXT="A compound number is anaphor where ... bioactivity is found in compounds [1-7,9-11] listed in Additional file 1...&#x2019;"/>
</node>
</node>
<node CREATED="1591888824477" ID="ID_279806442" MODIFIED="1592380791780" TEXT="Sum formula">
<node CREATED="1591889280708" FOLDED="true" ID="ID_1247208608" MODIFIED="1594038474726" TEXT="Consists of the elements contributing to">
<node CREATED="1591889449407" ID="ID_649897547" MODIFIED="1591889507327" TEXT="&#x2018;C9H8O4&#x2019;"/>
</node>
</node>
<node CREATED="1591888832284" ID="ID_127070337" MODIFIED="1592380792859" TEXT="Chemical structures">
<node CREATED="1591889301881" FOLDED="true" ID="ID_1943380317" MODIFIED="1594038475831" TEXT="explicit and implicit structures">
<node CREATED="1591889520511" ID="ID_1605536998" MODIFIED="1591889522705" TEXT="Markush structures, where R1 = CH3, COOH, etc..."/>
</node>
</node>
</node>
<node CREATED="1591889658859" FOLDED="true" ID="ID_1780431118" MODIFIED="1594038514478" POSITION="right" TEXT="Textual representations">
<node CREATED="1591889675582" ID="ID_202750671" MODIFIED="1592380915800" TEXT="Linguistic">
<node CREATED="1591889819781" ID="ID_93726852" MODIFIED="1591889822757" TEXT="to find the prefix that is common to all variations of the term,"/>
<node CREATED="1591889830824" ID="ID_518690890" MODIFIED="1591889833721" TEXT="to find the root term of the variant word,"/>
<node CREATED="1591889841236" ID="ID_1250900850" MODIFIED="1591889844405" TEXT="to assign each token to a grammatical category or"/>
<node CREATED="1591889850761" ID="ID_928997815" MODIFIED="1591889853885" TEXT="to divide the text into syntactical correlated parts of words,"/>
<node CREATED="1591889862985" ID="ID_1784800051" MODIFIED="1591889867190" TEXT="(e.g chucking, lemmatization, stemming and Part-of-speech (POS) tagging)"/>
</node>
<node CREATED="1591889687766" ID="ID_491618136" MODIFIED="1592380917718" TEXT="Ortographic">
<node CREATED="1591889740445" ID="ID_1053881598" MODIFIED="1591889742351" TEXT="to capture knowledge on word formation by the presence of these features, (e.g capitalization and symbols)"/>
</node>
<node CREATED="1591889704412" ID="ID_141902725" MODIFIED="1592380924751" TEXT="Morphological">
<node CREATED="1591889758256" ID="ID_859945283" MODIFIED="1591889762238" TEXT="to reflect common structures and/or sub-sequences of characters among entities, (e.g suffixes and prefixes, char n-gram and word shape patterns) "/>
</node>
<node CREATED="1591889709389" ID="ID_714309108" MODIFIED="1592380926667" TEXT="Context">
<node CREATED="1591889776930" ID="ID_1515723140" MODIFIED="1591889781417" TEXT="to establish a higher level of relationship between the tokens and the extracted features, e.g (windows and conjunctions) "/>
</node>
<node CREATED="1591889719343" ID="ID_813284704" MODIFIED="1592380927787" TEXT="Lexicon">
<node CREATED="1591889800717" ID="ID_218467646" MODIFIED="1591889806975" TEXT="to add domain knowledge to the set of features for optimizing the NER system. Dictionaries of domain term are used to match the entity names in the text and the resulting tags are used as features. Examples of the types of dictionaries used (target entity name and trigger name).  "/>
</node>
</node>
<node CREATED="1592468630502" FOLDED="true" ID="ID_884931672" MODIFIED="1594038439196" POSITION="left" TEXT="Formulaic language (not exclusive of  chemical entities)">
<node CREATED="1592468665413" ID="ID_1904492882" MODIFIED="1592468676624" TEXT="Semi-structured documents">
<node CREATED="1592468704011" ID="ID_769840119" MODIFIED="1592468749162" TEXT="Usually delimited by typographic conventions such as newlines and bold text, rather than formal markup"/>
</node>
<node CREATED="1592468677099" ID="ID_1758746991" MODIFIED="1592468686939" TEXT="Domain-specific entities">
<node CREATED="1592468756699" ID="ID_1603727005" MODIFIED="1592468780467" TEXT="Entities and terminology from different scientific domains"/>
</node>
<node CREATED="1592468687299" ID="ID_956604796" MODIFIED="1592468695040" TEXT="Stock phrases">
<node CREATED="1592468784022" ID="ID_1463258353" MODIFIED="1592468804622" TEXT="&apos;X was added to a flask&apos;"/>
</node>
<node CREATED="1592468695295" ID="ID_567313582" MODIFIED="1592468699526" TEXT="Data phrases">
<node CREATED="1592468810501" ID="ID_82555351" MODIFIED="1592468837971" TEXT="&apos;(0.259 g, 1.302 mmol, ca. 100%)&apos;"/>
</node>
</node>
</node>
</map>
