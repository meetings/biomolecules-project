<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1591961886393" ID="ID_1798038771" MODIFIED="1591962781086" TEXT="ChemNER">
<node CREATED="1591961912984" ID="ID_1982789345" MODIFIED="1591961923096" POSITION="right" TEXT="Dictionary-based"/>
<node CREATED="1591961928125" ID="ID_1422853513" MODIFIED="1591961943515" POSITION="left" TEXT="Rule-based"/>
<node CREATED="1591961946411" ID="ID_955296085" MODIFIED="1591961985770" POSITION="right" TEXT="Machine learning-based">
<node CREATED="1591961979702" ID="ID_17876161" MODIFIED="1591961982081" TEXT="SVM">
<node CREATED="1591962422868" ID="ID_1134362567" MODIFIED="1591962424472" TEXT="Tang, B., Feng, Y., Wang, X., Wu, Y., Zhang, Y., Jiang, M., ... &amp; Xu, H. (2015). A comparison of conditional random fields and structured support vector machines for chemical entity recognition in biomedical literature. Journal of cheminformatics, 7(S1), S8."/>
</node>
<node CREATED="1591961985771" ID="ID_1314099547" MODIFIED="1591961987695" TEXT="CRF">
<node CREATED="1591962062446" ID="ID_1937666399" MODIFIED="1591962064022" TEXT="Tang, B., Feng, Y., Wang, X., Wu, Y., Zhang, Y., Jiang, M., ... &amp; Xu, H. (2015). A comparison of conditional random fields and structured support vector machines for chemical entity recognition in biomedical literature. Journal of cheminformatics, 7(S1), S8."/>
<node CREATED="1591962125091" ID="ID_655922809" MODIFIED="1591962128515" TEXT="Campos, D., Matos, S., &amp; Oliveira, J. L. (2015). A document processing pipeline for annotating chemical entities in scientific documents. Journal of cheminformatics, 7(1), S7."/>
<node CREATED="1592236941627" ID="ID_1154148019" MODIFIED="1592236972449" TEXT="Munkhdalai, T., Li, M., Batsuren, K., Park, H. A., Choi, N. H., &amp; Ryu, K. H. (2015). Incorporating domain knowledge in chemical and biomedical named entity recognition with word representations. Journal of cheminformatics, 7(S1), S9."/>
</node>
<node CREATED="1591961990927" ID="ID_411100803" MODIFIED="1591961994505" TEXT="DL">
<node CREATED="1591961997366" ID="ID_1216111694" MODIFIED="1591962029601" TEXT="Awan, Z., Kahlke, T., Ralph, P. J., &amp; Kennedy, P. J. (2019). Chemical Named Entity Recognition with Deep Contextualized Neural Embeddings."/>
<node CREATED="1591962162382" ID="ID_1094341874" MODIFIED="1591962163609" TEXT="Luo, L., Yang, Z., Yang, P., Zhang, Y., Wang, L., Wang, J., &amp; Lin, H. (2018). A neural network approach to chemical and gene/protein entity recognition in patents. Journal of cheminformatics, 10(1), 65."/>
</node>
</node>
<node CREATED="1591961961334" ID="ID_635880652" MODIFIED="1591961975861" POSITION="left" TEXT="Hybrid">
<node CREATED="1591962525400" ID="ID_619990135" MODIFIED="1591962530287" TEXT="Campos, D., Matos, S., &amp; Oliveira, J. L. (2015). A document processing pipeline for annotating chemical entities in scientific documents. Journal of cheminformatics, 7(1), S7. "/>
</node>
<node CREATED="1591962185726" ID="ID_702210083" MODIFIED="1591962194143" POSITION="right" TEXT="Review and Survey">
<node CREATED="1591962238505" ID="ID_1625985990" MODIFIED="1591962240428" TEXT="Eltyeb, S., &amp; Salim, N. (2014). Chemical named entities recognition: a review on approaches and applications. Journal of cheminformatics, 6(1), 17."/>
<node CREATED="1591962308848" ID="ID_1171214210" MODIFIED="1591962310606" TEXT="Krallinger, M., Leitner, F., Rabal, O., Vazquez, M., Oyarzabal, J., &amp; Valencia, A. (2015). CHEMDNER: The drugs and chemical names extraction challenge. Journal of cheminformatics, 7(1), S1."/>
</node>
<node CREATED="1591962786326" ID="ID_1255732338" MODIFIED="1591962793153" POSITION="left" TEXT="Pre-processing">
<node CREATED="1591962796417" ID="ID_1547724564" MODIFIED="1591962803815" TEXT="Text cleaning"/>
<node CREATED="1591962806851" ID="ID_1493221076" MODIFIED="1591962812352" TEXT="Sentence splitting"/>
<node CREATED="1591962815231" ID="ID_1550023347" MODIFIED="1591962822034" TEXT="Tokenization"/>
</node>
<node CREATED="1591962843847" ID="ID_643475543" MODIFIED="1591962848116" POSITION="right" TEXT="Post-processing">
<node CREATED="1591962859857" ID="ID_1376882881" MODIFIED="1591962867050" TEXT="Tagging consistency"/>
<node CREATED="1591962869970" ID="ID_152574308" MODIFIED="1591962876626" TEXT="Abbreviation resolution"/>
<node CREATED="1591962879232" ID="ID_759604999" MODIFIED="1591962886029" TEXT="Bracket balance"/>
</node>
</node>
</map>
