<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1595939525206" ID="ID_1591840118" MODIFIED="1595939545840" TEXT="Event extraction">
<node CREATED="1595939538840" ID="ID_1270675507" MODIFIED="1595939544472" POSITION="right" TEXT="open-domain">
<node CREATED="1595939695338" ID="ID_498661930" MODIFIED="1595939699658" TEXT="Aims at detecting events from texts and in most cases, also clustering similar events via extracted event key- words. Event keywords refer to those words/phrases mostly describing an event, and sometimes keywords are further divided into triggers and arguments."/>
</node>
<node CREATED="1595939548086" ID="ID_839622101" MODIFIED="1595939552284" POSITION="left" TEXT="closed-domain">
<node CREATED="1595939626551" ID="ID_1074570826" MODIFIED="1595939646874" TEXT="Uses predefined event schema to discover and extract desired events of particular type from text. An event schema contains several event types and their corresponding event structures"/>
</node>
</node>
</map>
