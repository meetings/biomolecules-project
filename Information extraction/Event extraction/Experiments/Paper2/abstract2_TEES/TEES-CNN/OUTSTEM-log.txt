[14:25:53 04/06]	Opening log OUTSTEM-log.txt at Thu Jun  4 14:25:53 2020
####### Log opened at Thu Jun  4 14:25:53 2020 #######
Command line: classify.py -m /home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip -i /home/lzanella/abstract2.txt -o OUTSTEM
[14:25:53 04/06]	Classifying input /home/lzanella/abstract2.txt
[14:25:53 04/06]	Classifying with model /home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip
[14:25:53 04/06]	Initializing steps: ['LOAD', 'GENIA_SPLITTER', 'BANNER', 'BLLIP_BIO', 'STANFORD_CONVERT', 'SPLIT_NAMES', 'FIND_HEADS', 'SAVE']
[14:25:53 04/06]	Preprocessor output OUTSTEM-preprocessed.xml.gz does not exist
[14:25:53 04/06]	------------ Preprocessing ------------
[14:25:53 04/06]	Preprocessor steps: ['LOAD', 'GENIA_SPLITTER', 'BANNER', 'BLLIP_BIO', 'STANFORD_CONVERT', 'SPLIT_NAMES', 'FIND_HEADS', 'SAVE']
[14:25:53 04/06]	* Preprocessor:PROCESS(ENTER) *
[14:25:53 04/06]	=== ENTER STEP Preprocessor:PROCESS:LOAD ===
[14:25:53 04/06]	Running step LOAD with arguments {'corpusName': None, 'output': None, 'dataSetNames': None, 'extensions': None, 'input': '/home/lzanella/abstract2.txt'}
[14:25:53 04/06]	Converting ST-format to Interaction XML
[14:25:53 04/06]	Checking source directories: [{'path': '/home/lzanella/abstract2.txt', 'extensions': ['txt'], 'dataset': None}]
[14:25:53 04/06]	Reading /home/lzanella/abstract2.txt
[14:25:53 04/06]	1 documents
[14:25:53 04/06]	Resolving equivalences
[14:25:53 04/06]	Duplication created 0 new events (new total 0 events)
[14:25:53 04/06]	=== EXIT STEP LOAD time: 0:00:00.005272 ===
[14:25:53 04/06]	=== ENTER STEP Preprocessor:PROCESS:GENIA_SPLITTER ===
[14:25:53 04/06]	Running step GENIA_SPLITTER with arguments {'debug': False, 'input': <ElementTree object at 0x7f71e3e746d0>, 'postProcess': True, 'output': None}
[14:25:53 04/06]	Loading corpus <ElementTree object at 0x7f71e3e746d0>
[14:25:53 04/06]	Corpus file loaded
[14:25:53 04/06]	Running GENIA Sentence Splitter /home/lzanella/.tees/tools/geniass (Using post-processing)
[14:25:53 04/06]	Splitting Documents (TEES.d0): 100.00 % (0:0:0.000)
[14:25:53 04/06]	Sentence splitting created 10 sentences
[14:25:53 04/06]	Redivided 0 sentences
[14:25:53 04/06]	=== EXIT STEP GENIA_SPLITTER time: 0:00:00.176449 ===
[14:25:53 04/06]	=== ENTER STEP Preprocessor:PROCESS:BANNER ===
[14:25:53 04/06]	Running step BANNER with arguments {'elementName': 'entity', 'input': <ElementTree object at 0x7f71e3e746d0>, 'debug': False, 'output': None, 'splitNewlines': True, 'processElement': 'sentence'}
[14:25:53 04/06]	Loading corpus <ElementTree object at 0x7f71e3e746d0>
[14:25:53 04/06]	Corpus file loaded
[14:25:53 04/06]	Running BANNER /home/lzanella/.tees/tools/BANNER
[14:25:53 04/06]	BANNER command: java -cp /home/lzanella/.tees/tools/BANNER/bin:/home/lzanella/.tees/tools/BANNER/lib/* banner.eval.BANNER test /tmp/tmpzySiWy/banner_config.xml
[14:25:59 04/06]	BANNER time: 0:00:05.931990
[14:25:59 04/06]	Inserting entities
[14:25:59 04/06]	BANNER found 11 entities in 4 sentence-elements (10 sentences processed)
[14:25:59 04/06]	New entity-elements: 11 (Split 0 BANNER entities with newlines)
[14:25:59 04/06]	=== EXIT STEP BANNER time: 0:00:05.937740 ===
[14:25:59 04/06]	=== ENTER STEP Preprocessor:PROCESS:BLLIP_BIO ===
[14:25:59 04/06]	Running step BLLIP_BIO with arguments {'pathBioModel': 'AUTO', 'requireEntities': False, 'output': None, 'debug': False, 'input': <ElementTree object at 0x7f71e3e746d0>, 'parseName': 'McCC'}
[14:25:59 04/06]	BLLIP parser
[14:25:59 04/06]	Loading corpus <ElementTree object at 0x7f71e3e746d0>
[14:25:59 04/06]	Corpus file loaded
[14:25:59 04/06]	Parser does the tokenization
[14:25:59 04/06]	BLLIP parser at: /home/lzanella/.tees/tools/BLLIP/dmcc-bllip-parser-558adf6
[14:25:59 04/06]	Biomodel at: /home/lzanella/.tees/tools/BLLIP/biomodel
[14:25:59 04/06]	Running BLLIP parser with tokenization
[14:25:59 04/06]	1st Stage arguments: ['first-stage/PARSE/parseIt', '-l999', '-N50', '/home/lzanella/.tees/tools/BLLIP/biomodel/parser/']
[14:25:59 04/06]	2nd Stage arguments: ['second-stage/programs/features/best-parses', '-l', '/home/lzanella/.tees/tools/BLLIP/biomodel/reranker/features.gz', '/home/lzanella/.tees/tools/BLLIP/biomodel/reranker/weights.gz']
[14:26:06 04/06]	Parsing: 100.00 % (0:0:7.020)
[14:26:06 04/06]	Processed succesfully all sentences
[14:26:06 04/06]	Inserting parses
[14:26:06 04/06]	Inserting elements for (TEES.d0.s9): 100.00 % (0:0:0)
[14:26:06 04/06]	Penn parse statistics: {'tokens-parse': 311, 'sentences-with-penn-tree': 10, 'tokens-elements': 311, 'input-offsets': 0, 'input-tokens': 30, 'sentences-with-tokens': 10, 'sentences': 10, 'tokens-exact-match': 311}
[14:26:06 04/06]	Parsed 10 sentences
[14:26:06 04/06]	All sentences had a Penn tree
[14:26:06 04/06]	=== EXIT STEP BLLIP_BIO time: 0:00:07.062633 ===
[14:26:06 04/06]	=== ENTER STEP Preprocessor:PROCESS:STANFORD_CONVERT ===
[14:26:06 04/06]	Running step STANFORD_CONVERT with arguments {'debug': False, 'outputFormat': None, 'input': <ElementTree object at 0x7f71e3e746d0>, 'parserName': 'McCC', 'memory': None, 'action': 'convert', 'output': None}
[14:26:06 04/06]	Loading corpus <ElementTree object at 0x7f71e3e746d0>
[14:26:06 04/06]	Corpus file loaded
[14:26:06 04/06]	Running Stanford conversion
[14:26:06 04/06]	Stanford tools at: /home/lzanella/.tees/tools/stanford-parser-2012-03-09
[14:26:06 04/06]	Stanford tools arguments: java -mx500m -cp stanford-parser.jar edu.stanford.nlp.trees.EnglishGrammaticalStructure -encoding utf8 -CCprocessed -keepPunct -treeFile
[14:26:08 04/06]	Stanford (convert): 100.00 % (0:0:2.005)
[14:26:08 04/06]	Processed succesfully all sentences
[14:26:08 04/06]	Inserting elements for (TEES.d0.s9): 100.00 % (0:0:0)
[14:26:08 04/06]	Dependency parse statistics: {'deps-total': 277, 'dep-tokens-total': 254, 'sentences-with-deps': 10, 'sentences': 10, 'deps-root': 10, 'dep-tokens-aligned': 254, 'deps-elements': 267}
[14:26:08 04/06]	Warning, 10 dependencies could not be aligned
[14:26:08 04/06]	=== EXIT STEP STANFORD_CONVERT time: 0:00:02.049557 ===
[14:26:08 04/06]	=== ENTER STEP Preprocessor:PROCESS:SPLIT_NAMES ===
[14:26:08 04/06]	Running step SPLIT_NAMES with arguments {'input': <ElementTree object at 0x7f71e3e746d0>, 'parseName': 'McCC', 'removeOld': True, 'output': None}
[14:26:08 04/06]	Protein Name Splitter
[14:26:08 04/06]	Splitting names (TEES.d0.s9): 100.00 % (0:0:0.007)
[14:26:08 04/06]	Tokenization missing from 0 sentences
[14:26:08 04/06]	=== EXIT STEP SPLIT_NAMES time: 0:00:00.008488 ===
[14:26:08 04/06]	=== ENTER STEP Preprocessor:PROCESS:FIND_HEADS ===
[14:26:08 04/06]	Running step FIND_HEADS with arguments {'parse': 'McCC', 'removeExisting': True, 'input': <ElementTree object at 0x7f71e3e746d0>, 'output': None}
[14:26:08 04/06]	Removing existing head offsets
[14:26:08 04/06]	Removed head offsets from 0 entities
[14:26:08 04/06]	Determining head offsets using parse McCC and tokenization None
[14:26:08 04/06]	1 documents, 10 sentences
[14:26:08 04/06]	Making sentence graphs (TEES.d0.s9): 100.00 % (0:0:0.005)
[14:26:08 04/06]	Skipped 0 duplicate interaction edges in SentenceGraphs
[14:26:08 04/06]	=== EXIT STEP FIND_HEADS time: 0:00:00.015889 ===
[14:26:08 04/06]	=== ENTER STEP Preprocessor:PROCESS:SAVE ===
[14:26:08 04/06]	Running step SAVE with arguments {'input': <ElementTree object at 0x7f71e3e746d0>, 'output': 'OUTSTEM-preprocessed.xml.gz'}
[14:26:08 04/06]	Writing output to OUTSTEM-preprocessed.xml.gz
[14:26:08 04/06]	=== EXIT STEP SAVE: 0:00:00.074459 ===
[14:26:08 04/06]	* Preprocessor:PROCESS(EXIT) 0:00:15.331379 *
[14:26:08 04/06]	=== EXIT STEP PREPROCESS time: 0:00:15.333048 ===
[14:26:08 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "TEES_MODEL_VALUES.tsv" to "/tmp/tmphghi1C/TEES_MODEL_VALUES.tsv"
[14:26:08 04/06]	Importing detector Detectors.KerasEventDetector
[14:26:09 04/06]	Using TensorFlow backend.
[14:26:21 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "TEES_MODEL_VALUES.tsv" to "/tmp/tmpyW6ceF/TEES_MODEL_VALUES.tsv"
[14:26:21 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "TEES_MODEL_VALUES.tsv" to "/tmp/tmpL4Qe2q/TEES_MODEL_VALUES.tsv"
[14:26:21 04/06]	Keras components: {'unmerging': True, 'modifier': True, 'trigger': True, 'edge': True}
[14:26:21 04/06]	* KerasEventDetector:CLASSIFY(ENTER) *
[14:26:21 04/06]	=== ENTER STEP KerasEventDetector:CLASSIFY:TRIGGERS ===
[14:26:21 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "TEES_MODEL_VALUES.tsv" to "/tmp/tmpx014Hp/TEES_MODEL_VALUES.tsv"
[14:26:21 04/06]	Path depth is 4
[14:26:21 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "structure.txt" to "/tmp/tmpx014Hp/structure.txt"
[14:26:21 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "entity-embeddings.json" to "/tmp/tmpx014Hp/entity-embeddings.json"
[14:26:21 04/06]	Loading embedding indices from /tmp/tmpx014Hp/entity-embeddings.json
[14:26:21 04/06]	[(u'POS', 47), (u'named_entities', 4), (u'path0', 361), (u'path1', 362), (u'path2', 362), (u'path3', 362), (u'positions', 43), (u'words', 11161)]
[14:26:21 04/06]	Building examples with styles: {'do': ['0.1', '0.2', '0.5'], 'mods': '20', 'dense': ['400', '800'], 'keras': True, 'nf': ['256', '512'], 'epochs': '500', 'patience': '10', 'el': '41', 'path': ['0', '2', '4']}
[14:26:21 04/06]	Example generation for set classification
[14:26:21 04/06]	[input, gold] = [<ElementTree object at 0x7f71e3e746d0>, <ElementTree object at 0x7f71e3e746d0>]
[14:26:21 04/06]	Building examples (TEES.d0.s9): 10 (0:0:0) 
[14:26:21 04/06]	Examples built: 102
[14:26:21 04/06]	Example Statistics (total/filtered)
[14:26:21 04/06]	  : 108/6 {'name': 6}
[14:26:21 04/06]	  Protein: 11/11 {'name': 11}
[14:26:21 04/06]	Positives Coverage 85.71 % [119, 17]
[14:26:21 04/06]	Padding examples to length: 41
[14:26:21 04/06]	{'examples-classification': 102}
[14:26:21 04/06]	TEES.d0.s0.x0 []
[14:26:21 04/06]	['index', u'POS', u'named_entities', u'path0', u'path1', u'path2', u'path3', u'positions', u'words']
[14:26:21 04/06]	[0, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[1, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[2, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[3, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[4, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[5, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[6, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[7, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[8, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[9, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[10, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[11, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[12, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[13, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[14, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[15, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[16, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[17, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[18, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[19, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[20, u'NN', u'0', u'[d0]', u'[d0]', u'[d0]', u'[d0]', u'20', u'[out]']
[14:26:21 04/06]	[21, u'IN', u'0', u'[unconnected]', u'[unconnected]', u'[unconnected]', u'[unconnected]', u'[out]', u'of']
[14:26:21 04/06]	[22, u'JJ', u'0', u'prep_of>', u'amod>', u'[N/A]', u'[N/A]', u'[out]', u'antioxidant']
[14:26:21 04/06]	[23, u'NN', u'0', u'prep_of>', u'nn>', u'[N/A]', u'[N/A]', u'[out]', u'[out]']
[14:26:21 04/06]	[24, u'NNS', u'0', u'prep_of>', u'[N/A]', u'[N/A]', u'[N/A]', u'[out]', u'derivatives']
[14:26:21 04/06]	[25, u'IN', u'0', u'[unconnected]', u'[unconnected]', u'[unconnected]', u'[unconnected]', u'[out]', u'in']
[14:26:21 04/06]	[26, u'JJ', u'0', u'prep_in>', u'nn>', u'amod>', u'[N/A]', u'[out]', u'[out]']
[14:26:21 04/06]	[27, u'NN', u'1', u'prep_in>', u'nn>', u'[N/A]', u'[N/A]', u'[out]', u'poly']
[14:26:21 04/06]	[28, u'(', u'0', u'prep_in>', u'nn>', u'appos>', u'punct>', u'[out]', u'(']
[14:26:21 04/06]	[29, u'NN', u'0', u'prep_in>', u'nn>', u'appos>', u'[N/A]', u'[out]', u'[out]']
[14:26:21 04/06]	[30, u')', u'0', u'prep_in>', u'nn>', u'appos>', u'punct>', u'[out]', u')']
[14:26:21 04/06]	[31, u'NN', u'0', u'prep_in>', u'nn>', u'[N/A]', u'[N/A]', u'[out]', u'[out]']
[14:26:21 04/06]	[32, u'NN', u'0', u'prep_in>', u'nn>', u'dep>', u'[N/A]', u'[out]', u'[out]']
[14:26:21 04/06]	[33, u'(', u'0', u'prep_in>', u'punct>', u'[N/A]', u'[N/A]', u'[out]', u'(']
[14:26:21 04/06]	[34, u'NN', u'0', u'prep_in>', u'nn>', u'[N/A]', u'[N/A]', u'[out]', u'[out]']
[14:26:21 04/06]	[35, u')', u'0', u'prep_in>', u'punct>', u'[N/A]', u'[N/A]', u'[out]', u')']
[14:26:21 04/06]	[36, u'NNS', u'0', u'prep_in>', u'[N/A]', u'[N/A]', u'[N/A]', u'[out]', u'[out]']
[14:26:21 04/06]	[37, u'.', u'0', u'punct>', u'[N/A]', u'[N/A]', u'[N/A]', u'[out]', u'.']
[14:26:21 04/06]	[38, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[39, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	[40, u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]', u'[pad]']
[14:26:21 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "entity-labels.json" to "/tmp/tmpx014Hp/entity-labels.json"
[14:26:21 04/06]	Vectorizing labels [u'Binding', u'Entity', u'Gene_expression', u'Localization', u'Negative_regulation', u'Phosphorylation', u'Positive_regulation', u'Protein_catabolism', u'Regulation', u'Transcription']
[14:26:21 04/06]	Vectorizing features: [u'POS', u'named_entities', u'path0', u'path1', u'path2', u'path3', u'positions', u'words']
[14:26:21 04/06]	POS (102, 41) [ 1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  2  3  4  2
[14:26:21 04/06]	 10  3  4  2  7  2  9  2  2  7  2  9 10 11  1  1  1]
[14:26:21 04/06]	named_entities (102, 41) [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 3 2 2 2 2 2 2 2 2 2
[14:26:21 04/06]	 2 1 1 1]
[14:26:21 04/06]	path0 (102, 41) [  1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
[14:26:21 04/06]	   1   1 358 359   7   7   7 359  15  15  15  15  15  15  15  15  15  15
[14:26:21 04/06]	  15 261   1   1   1]
[14:26:21 04/06]	path1 (102, 41) [  1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
[14:26:21 04/06]	   1   1 358 359   5  19 360 359  19  19  19  19  19  19  19 261  19 261
[14:26:21 04/06]	 360 360   1   1   1]
[14:26:21 04/06]	path2 (102, 41) [  1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
[14:26:21 04/06]	   1   1 358 359 360 360 360 359   5 360  27  27  27 360  61 360 360 360
[14:26:21 04/06]	 360 360   1   1   1]
[14:26:21 04/06]	path3 (102, 41) [  1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1
[14:26:21 04/06]	   1   1 358 359 360 360 360 359 360 360 261 360 261 360 360 360 360 360
[14:26:21 04/06]	 360 360   1   1   1]
[14:26:21 04/06]	positions (102, 41) [ 1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1  1 22  0  0  0
[14:26:21 04/06]	  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  1  1]
[14:26:21 04/06]	words (102, 41) [   1    1    1    1    1    1    1    1    1    1    1    1    1    1
[14:26:21 04/06]	    1    1    1    1    1    1    0    3 3315    0 2589   13    0  843
[14:26:21 04/06]	    8    0   10    0    0    8    0   10    0   26    1    1    1]
[14:26:21 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "entity-models.json" to "/tmp/tmpx014Hp/entity-models.json"
[14:26:21 04/06]	Predicting with model 1 entity-model-15.hdf5
[14:26:21 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "entity-model-15.hdf5" to "/tmp/tmpx014Hp/entity-model-15.hdf5"
[14:26:21 04/06]	/home/lzanella/orpailleur/lzanella/anaconda3/envs/py27/lib/python2.7/site-packages/keras/engine/topology.py:1252: UserWarning: The `Merge` layer is deprecated and will be removed after 08/2017. Use instead layers from `keras.layers.merge`, e.g. `add`, `concatenate`, etc.
[14:26:21 04/06]	  return cls(**config)
[14:26:23 04/06]	102/102 [==============================] - 0s     
[14:26:23 04/06]	
[14:26:23 04/06]	***** Results for ensemble, size = 1 *****
[14:26:23 04/06]	Evaluating, labels = [u'Binding', u'Entity', u'Gene_expression', u'Localization', u'Negative_regulation', u'Phosphorylation', u'Positive_regulation', u'Protein_catabolism', u'Regulation', u'Transcription']
[14:26:23 04/06]	/home/lzanella/.local/lib/python2.7/site-packages/sklearn/metrics/classification.py:1143: UndefinedMetricWarning: Precision and F-score are ill-defined and being set to 0.0 in labels with no predicted samples.
[14:26:23 04/06]	  'precision', 'predicted', average, warn_for)
[14:26:23 04/06]	/home/lzanella/.local/lib/python2.7/site-packages/sklearn/metrics/classification.py:1145: UndefinedMetricWarning: Recall and F-score are ill-defined and being set to 0.0 in labels with no true samples.
[14:26:23 04/06]	  'recall', 'true', average, warn_for)
[14:26:23 04/06]	Binding prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Entity prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Gene_expression prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Localization prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Negative_regulation prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Phosphorylation prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Positive_regulation prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Protein_catabolism prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Regulation prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	Transcription prfs = (0.0, 0.0, 0.0, 0)
[14:26:23 04/06]	/home/lzanella/.local/lib/python2.7/site-packages/sklearn/metrics/classification.py:1143: UndefinedMetricWarning: Precision and F-score are ill-defined and being set to 0.0 due to no predicted samples.
[14:26:23 04/06]	  'precision', 'predicted', average, warn_for)
[14:26:23 04/06]	/home/lzanella/.local/lib/python2.7/site-packages/sklearn/metrics/classification.py:1145: UndefinedMetricWarning: Recall and F-score are ill-defined and being set to 0.0 due to no true samples.
[14:26:23 04/06]	  'recall', 'true', average, warn_for)
[14:26:23 04/06]	micro prfs =  (0.0, 0.0, 0.0, None)
[14:26:23 04/06]	1 documents, 10 sentences
[14:26:23 04/06]	Making sentence graphs (TEES.d0.s9): 100.00 % (0:0:0.004)
[14:26:23 04/06]	Skipped 0 duplicate interaction edges in SentenceGraphs
[14:26:23 04/06]	Writing examples (TEES.d0.s9.x101): 100.00 % (0:0:0)
[14:26:23 04/06]	Writing corpus to /tmp/tmpbySSX4/OUTSTEM-entity-pred.xml.gz
[14:26:23 04/06]	=== EXIT STEP TRIGGERS time: 0:00:02.085930 ===
[14:26:23 04/06]	=== ENTER STEP KerasEventDetector:CLASSIFY:EDGES ===
[14:26:23 04/06]	Path depth is 4
[14:26:23 04/06]	Caching model "/home/lzanella/orpailleur/lzanella/EE_tools/TEES/GE09-single.zip" member "edge-embeddings.json" to "/tmp/tmpx014Hp/edge-embeddings.json"
[14:26:23 04/06]	Loading embedding indices from /tmp/tmpx014Hp/edge-embeddings.json
[14:26:23 04/06]	[(u'POS', 47), (u'entities', 30), (u'path1_0', 361), (u'path1_1', 362), (u'path1_2', 362), (u'path1_3', 362), (u'path2_0', 361), (u'path2_1', 362), (u'path2_2', 362), (u'path2_3', 362), (u'positions1', 328), (u'positions2', 327), (u'rel_token', 7), (u'sp_in', 361), (u'sp_mask', 4), (u'sp_out', 361), (u'words', 11161)]
[14:26:23 04/06]	Building examples with styles: {'do': ['0.1', '0.2', '0.5'], 'mods': '20', 'dense': ['400', '800'], 'keras': True, 'nf': ['256', '512'], 'epochs': '500', 'patience': '10', 'ol': '15', 'path': ['0', '2', '4']}
[14:26:23 04/06]	Example generation for set classification
[14:26:23 04/06]	[input, gold] = [<ElementTree object at 0x7f71e3e746d0>, <ElementTree object at 0x7f71e3e746d0>]
[14:26:23 04/06]	Building examples (TEES.d0.s9): 10 (0:0:0) 
[14:26:23 04/06]	Examples built: 0
[14:26:23 04/06]	Example Statistics (total/filtered)
[14:26:23 04/06]	  : 36/36 {'auto_limits': 36}
[14:26:23 04/06]	Positives Coverage 0.00 % [36, 36]
[14:26:23 04/06]	Duplicate entities skipped: 0
[14:26:23 04/06]	Padding examples to length: 82
[14:26:23 04/06]	{}
[14:26:23 04/06]	No examples to classify
[14:26:23 04/06]	Traceback (most recent call last):
[14:26:23 04/06]	  File "classify.py", line 163, in <module>
[14:26:23 04/06]	    preprocessorParams=options.preprocessorParams, bioNLPSTParams=options.bioNLPSTParams)
[14:26:23 04/06]	  File "classify.py", line 79, in classify
[14:26:23 04/06]	    detector.classify(classifyInput, model, output, goldData=goldInput, fromStep=detectorSteps["CLASSIFY"], omitSteps=omitDetectorSteps["CLASSIFY"], workDir=workDir)
[14:26:23 04/06]	  File "/srv/storage/orpailleur@talc-data2.nancy.grid5000.fr/lzanella/EE_tools/TEES/Detectors/KerasEventDetector.py", line 267, in classify
[14:26:23 04/06]	    EventDetector.classify(self, data, model, output, parse=parse, task=task, goldData=goldData, fromStep=fromStep, toStep=toStep, omitSteps=omitSteps, workDir=workDir)
[14:26:23 04/06]	  File "/srv/storage/orpailleur@talc-data2.nancy.grid5000.fr/lzanella/EE_tools/TEES/Detectors/EventDetector.py", line 346, in classify
[14:26:23 04/06]	    assert xml != None
[14:26:23 04/06]	AssertionError
