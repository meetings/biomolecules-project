import spacy 
nlp = spacy.load("en_core_web_sm")

# Print the names of the pipeline components
print('List of component names: ', nlp.pipe_names)

# Print the full pipeline of (name, component) tuples
print('List of (name, component) tuples: ', nlp.pipeline)

